class AppConstant {
  static const practiceScreenRouteName = '/practice';
  static const doctorScreenRouteName = '/doctor';
  static const staffDefaultScreenRouteName = '/staff_default';
  static const staffNurseScreenRouteName = '/staff_nurse';

}
