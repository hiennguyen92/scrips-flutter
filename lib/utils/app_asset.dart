class AppImages {
  static const icEmpty = 'assets/base/icons/ic_empty.png';
  static const emptyAvatar = 'assets/app/icons/empty_avatar.png';
  static const dubaiHospital = 'assets/app/icons/hospital_dubai.png';
  static const abuDhabiHospital = 'assets/app/icons/hospital_abudhabi.png';
  static const oliviaStaff = 'assets/app/icons/staff_olivia.png';
  static const nadineStaff = 'assets/app/icons/staff_nadine.png';
  static const barbaraStaff = 'assets/app/icons/staff_barbara.png';
  static const rebeccaStaff = 'assets/app/icons/staff_rebecca.png';
  static const jackDoctor = 'assets/app/icons/doctor_jack.png';
  static const janeDoctor = 'assets/app/icons/doctor_jane.png';
  static const johnDoctor = 'assets/app/icons/doctor_john.png';
  static const amandaDoctor = 'assets/app/icons/doctor_amanda.png';
  static const icCalendar = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_calendar.png';
  static const icDropdown = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_drop_down.png';
  static const icCamera = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_camera.png';
  static const icSmallClock = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_small_clock.png';
  static const icLocation = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_location.png';
  static const icBreadCrumbNext = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_breadcrumb_next.png';
  static const icBack = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_back.png';
  static const icUserPic = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_userpic.png';
  static const icHolidayTime = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_holiday_time.png';
  static const icWorkTime = 'submodules/scrips_msp1_flutter_shared/scrips_core/assets/ic_work_time.png';

}

class AppVideos {}

class AppFonts {
  static const String Roboto = 'Roboto';
}
