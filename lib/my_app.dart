import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/constants/app_assets.dart';
import 'package:scrips_core/di/dependency_injection.dart';
import 'package:scripsf/pages/doctors/doctor_screen.dart';
import 'package:scripsf/pages/home/home_screen.dart';
import 'package:scripsf/pages/practices/practice_screen.dart';
import 'package:scripsf/pages/staffs/staff_default_screen.dart';
import 'package:scripsf/pages/staffs/staff_nurse_screen.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_constant.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
//    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

final getIt = GetIt.instance;

void diSetup() {
  getIt.registerSingleton<LocalStorage>(LocalStorage());
}

Future<void> myMain() async {
  // Ensure initialized
  WidgetsFlutterBinding.ensureInitialized();

  // Init scrips core
  await initCoreServiceLocator();

  // Get it setup
  diSetup();

  // Debug bloc
  BlocSupervisor.delegate = SimpleBlocDelegate();
  // Run Application
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: Fonts.roboto,
      ),
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case AppConstant.practiceScreenRouteName:
            return FadeRoute(page: PracticeScreen());
          case AppConstant.doctorScreenRouteName:
            return FadeRoute(page: DoctorScreen());
          case AppConstant.staffDefaultScreenRouteName:
            return FadeRoute(page: StaffDefaultScreen());
          case AppConstant.staffNurseScreenRouteName:
            return FadeRoute(page: StaffNurseScreen());
        }
        return FadeRoute(page: HomeScreen());
      },
    );
  }
}

class FadeRoute<T> extends MaterialPageRoute<T> {
  FadeRoute({ Widget page, RouteSettings settings })
      : super(builder: (_) => page, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}
