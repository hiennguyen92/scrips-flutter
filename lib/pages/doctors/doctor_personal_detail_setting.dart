import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/utils/utils.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

import '../home/home_doctor_tab.dart';

class DoctorPersonalDetailSetting extends StatelessWidget {
  DoctorPersonalDetailSetting({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<DoctorPersonalDetailBloc, DoctorItem>(
        builder: (context, doctorItem) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 8, bottom: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormView(
                          margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
                          header: "PERSONAL DETAILS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text("ACCOUNT PHOTO",
                                    style: boldLabelTextStyle(12.0, labelTextStyleTextColor)),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8, bottom: 25.5),
                                child: Stack(
                                  children: [
                                    Container(
                                        width: 72.0,
                                        height: 72.0,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: AssetImage(
                                                    doctorItem.avatar ?? AppImages.icUserPic)))),
                                    Container(
                                      width: 24,
                                      child: Image.asset(AppImages.icCamera),
                                    )
                                  ],
                                ),
                              ),
                              FieldAndLabel(
                                labelValue: 'FIRST NAME',
                                placeholder: 'Enter first name',
                                isMandatory: true,
                                fieldValue: 'Jack',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'MIDDLE NAME',
                                placeholder: 'Enter middle name (Optional)',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'LAST NAME',
                                placeholder: 'Enter last name',
                                isMandatory: true,
                                fieldValue: 'Smith',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'DATE OF BIRTH',
                                placeholder: 'Select birth date',
                                fieldValue: 'Jan 23, 2018',
                                fieldType: FieldType.DateRangePicker,
                                icon: Image.asset(AppImages.icCalendar),
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                isMandatory: true,
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                fieldValue: 'male',
                                tagsItems: [
                                  ValueDisplayPair('male', 'Male'),
                                  ValueDisplayPair('female', 'Female'),
                                  ValueDisplayPair('other', 'Other')
                                ],
                                fieldTextColor: textInputColor,
                                fieldType: FieldType.SingleTagPicker,
                                labelTextStyle: defaultFieldLabelStyle(null, null),
                                labelValue: "GENDER".toUpperCase(),
                                validationMessage: "",
                                axis: Axis.vertical,
                                enabled: true,
                              ),
                            ],
                          )),
                      Space(vertical: 24),
                      FormView(
                          margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
                          header: "CONTACT DETAILS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FieldAndLabel(
                                labelValue: 'ADDRESS',
                                placeholder: 'Enter address',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'COUNTRY',
                                placeholder: 'Select country',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'STATE',
                                placeholder: 'Select state',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'CITY',
                                placeholder: 'Select city',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'PO BOX',
                                placeholder: 'Enter PO box',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'CONTACT NUMBER',
                                isMandatory: true,
                                validationMessage: "",
                                placeholder: "e.g. +971 23 345 6789",
                                fieldValue: "",
                                axis: Axis.vertical,
                                enabled: true,
                                margin: EdgeInsets.all(0),
                                fieldType: FieldType.PhoneField,
                                maxLength: 20,
                                country: null,
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<DoctorPersonalDetailBloc>(context).add(true);
  }
}

class DoctorPersonalDetailBloc extends Bloc<bool, DoctorItem> {
  final DoctorItem initialItem = DoctorItem(
      name: 'Empty',
      department: 'department',
      info: 'info',
      isPrimary: false,
      avatar: AppImages.icEmpty);

  @override
  DoctorItem get initialState => initialItem;

  @override
  Stream<DoctorItem> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getDoctorInfo();
  }
}
