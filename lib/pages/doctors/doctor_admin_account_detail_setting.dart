import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/resizable_cupertino_switch_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

import '../home/home_doctor_tab.dart';

class DoctorAdminAccountDetailSetting extends StatelessWidget {
  DoctorAdminAccountDetailSetting({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<DoctorAdminAccountDetailBloc, DoctorItem>(
        builder: (context, doctorItem) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Row(
                    children: [
                      Text('ACCOUNT ID: ',
                          style:
                              boldLabelTextStyle(12, labelTextStyleTextColor)),
                      Text('172839',
                          style: normalLabelTextStyle(13, regularTextColor)),
                      SizedBox(width: 23),
                      Text('LAST SIGN-IN: ',
                          style:
                              boldLabelTextStyle(12, labelTextStyleTextColor)),
                      Text('Apr 30, 2020',
                          style: normalLabelTextStyle(13, regularTextColor)),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'Resend Invite',
                            style:
                                semiBoldLabelTextStyle(13, enabledBtnBGColor),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 36, bottom: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormView(
                          margin: EdgeInsets.only(
                              top: 8.5, bottom: 8.5, right: 26, left: 16),
                          header: "ACCOUNT STATUS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Set account active',
                                        style: normalLabelTextStyle(
                                            17, regularTextColor)),
                                    SizedCupertinoSwitch(
                                        height: 31,
                                        width: 51,
                                        scale: 1,
                                        value: true)
                                  ],
                                ),
                              ),
                            ],
                          )),
                      Space(vertical: 8),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                            'Enabling ‘Set account active’ will let the user log in into the system. We recommend to keep the account active at all times as long as the user is still a member of your organization.',
                            style: semiBoldLabelTextStyle(
                                13, labelTextStyleTextColor)),
                      ),
                      Space(vertical: 28),
                      FormView(
                          margin: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 26),
                          header: "ACCOUNT DETAILS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FieldAndLabel(
                                labelValue: 'ROLE',
                                isMandatory: true,
                                fieldValue: 'In-house Doctor',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: FieldAndLabel(
                                        labelValue: 'EMAIL ADDRESS',
                                        placeholder: 'Enter email address',
                                        isMandatory: true,
                                        fieldValue:
                                            'jack.smith@dubaiprimary.ae',
                                        margin: EdgeInsets.all(0),
                                        padding: EdgeInsets.only(left: 0),
                                        fieldBackgroundColor: textFieldBGcolor,
                                        labelTextStyle: boldLabelTextStyle(
                                            12, labelTextStyleTextColor),
                                      ),
                                    ),
                                  ),
                                  Button(
                                    height: 36,
                                    width: 70,
                                    buttonBackgroundColor: disabledBtnBGColor,
                                    text: 'Update',
                                    style: semiBoldLabelTextStyle(
                                        15, enabledBtnTextColor),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Button(
                    height: 48,
                    radius: 13,
                    buttonBackgroundColor: enabledBtnTextColor,
                    style: semiBoldLabelTextStyle(17, redButtonTextColor),
                    text: "Archive This Doctor",
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 16),
                  child: RichText(
                    text: TextSpan(
                      text:
                          'Archiving will remove this doctor from active view but will not delete the doctor. You can find archived doctors  in the ',
                      style:
                          semiBoldLabelTextStyle(13, labelTextStyleTextColor),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Archive',
                            style:
                                semiBoldLabelTextStyle(13, normalBtnTextColor)),
                        TextSpan(text: ' and unarchive, if necessary.'),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<DoctorAdminAccountDetailBloc>(context).add(true);
  }
}

class DoctorAdminAccountDetailBloc extends Bloc<bool, DoctorItem> {
  final DoctorItem initialItem = DoctorItem(
      name: 'Empty',
      department: 'department',
      info: 'info',
      isPrimary: false,
      avatar: AppImages.icEmpty);

  @override
  DoctorItem get initialState => initialItem;

  @override
  Stream<DoctorItem> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getDoctorInfo();
  }
}
