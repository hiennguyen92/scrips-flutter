import 'package:flutter/material.dart';
import 'package:scripsf/pages/home/home_account_tab.dart';
import 'package:scripsf/widgets/blank_setting.dart';
import 'package:scripsf/pages/home/home_contact_tab.dart';
import 'package:scripsf/pages/home/home_doctor_tab.dart';
import 'package:scripsf/pages/practices/practice_detail_tab.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/pages/home/home_role_tab.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/app_content.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

import 'doctor_admin_account_detail_setting.dart';
import 'doctor_personal_detail_setting.dart';

class DoctorScreen extends StatelessWidget {

  final List<TabItem> doctorDetailTab = <TabItem>[
    TabItem(title: 'Personal Details', view: DoctorPersonalDetailSetting()),
    TabItem(
      title: 'Account Details',
      view: DoctorAdminAccountDetailSetting(),
//        view: DoctorDefaultAccountDetailSetting(),
    ),
    TabItem(title: 'Professional Details', view: BlankSetting()),
    TabItem(title: 'Schedule', view: BlankSetting()),
    TabItem(title: 'Practices', view: BlankSetting()),
  ];

  @override
  Widget build(BuildContext context) {
    return AppContent(screen: TabScreen(tabs: doctorDetailTab));
  }
}
