import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/constants/app_assets.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/utils/utils.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/resizable_cupertino_switch_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

class PracticeDetailTab extends StatelessWidget {
  PracticeDetailTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<PracticeDetailBloc, PracticeItem>(
        builder: (context, practiceItem) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 8, bottom: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormView(
                          margin: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 24),
                          header: "PRACTICE DETAILS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text("PRACTICE PHOTO",
                                    style: boldLabelTextStyle(
                                        12.0, labelTextStyleTextColor)),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8, bottom: 25.5),
                                child: Stack(
                                  children: [
                                    Container(
                                        width: 80.0,
                                        height: 80.0,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image: AssetImage(
                                                    practiceItem.avatar ??
                                                        AppImages.icEmpty)))),
                                    Container(
                                      width: 24,
                                      child: Image.asset(AppImages.icCamera),
                                    )
                                  ],
                                ),
                              ),
                              FieldAndLabel(
                                labelValue: 'PRACTICE NAME',
                                placeholder: 'Enter practice name',
                                isMandatory: true,
                                fieldValue: 'Dubai Primary',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'LICENSE NUMBER',
                                placeholder: 'Enter license number',
                                isMandatory: true,
                                fieldValue: 'LN12345678',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                isMandatory: true,
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                fieldValue: 'DHA',
                                tagsItems: [
                                  ValueDisplayPair('DHA', 'DHA'),
                                  ValueDisplayPair('HAAD', 'HAAD'),
                                  ValueDisplayPair('MOH', 'MOH'),
                                  ValueDisplayPair('EHA', 'EHA'),
                                  ValueDisplayPair('DHCC', 'DHCC')
                                ],
                                fieldTextColor: textInputColor,
                                fieldType: FieldType.SingleTagPicker,
                                labelTextStyle: defaultFieldLabelStyle(null, null),
                                labelValue: "LICENSE ISSUING AUTHORITY",
                                validationMessage: "",
                                axis: Axis.vertical,
                                enabled: true,
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'LICENSE EXPIRATION DATE',
                                placeholder: 'Select expiration date',
                                fieldValue: 'Jan 23, 2018',
                                fieldType: FieldType.DateRangePicker,
                                icon: Image.asset(AppImages.icCalendar),
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'BILLING TAX ID',
                                isMandatory: false,
                                placeholder: 'e.g. 123456-12 (if applicable)',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'PRACTICE DESCRIPTION',
                                placeholder: 'Enter practice description',
                                isMandatory: false,
                                fieldType: FieldType.RichTextEdit,
                                fieldValue:
                                'Primary Clinic Dubai provides family practice services in neurology, ophthalmology, cardiology, allergy, obstetrics & gynecology. The clinics are staffed by six family physicians,',
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Container(
                                  margin:
                                  EdgeInsets.only(top: 16, bottom: 22.5),
                                  child: Text(
                                    'Practice Description is what patients see in the profile of a practice while booking.',
                                    style: TextStyle(
                                        color: labelTextStyleTextColor,
                                        fontSize: 13,
                                        fontFamily: Fonts.roboto),
                                  )),
                              Container(
                                height: 48,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Make this practice primary',
                                        style: normalLabelTextStyle(
                                            17, regularTextColor)),
                                    SizedCupertinoSwitch(
                                        height: 31,
                                        width: 51,
                                        scale: 1,
                                        value: true)
                                  ],
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Button(
                    height: 48,
                    radius: 13,
                    buttonBackgroundColor: enabledBtnTextColor,
                    style: semiBoldLabelTextStyle(17, redButtonTextColor),
                    text: "Archive/Unarchive This Practice",
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 16),
                  child: RichText(
                    text: TextSpan(
                      text:
                      'Archiving will remove this practice from active view but will not delete the practice. You can find archived practices in the ',
                      style:
                      semiBoldLabelTextStyle(13, labelTextStyleTextColor),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Archive',
                            style: semiBoldLabelTextStyle(
                                13, normalBtnTextColor)),
                        TextSpan(text: ' and unarchive, if necessary.'),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<PracticeDetailBloc>(context).add(true);
  }
}

class PracticeDetailBloc extends Bloc<bool, PracticeItem> {
  final PracticeItem initialItem = PracticeItem(
      title: '',
      location: '',
      time: '',
      isPrimary: false,
      avatar: AppImages.icEmpty);

  @override
  PracticeItem get initialState => initialItem;

  @override
  Stream<PracticeItem> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getPracticeInfo();
  }
}
