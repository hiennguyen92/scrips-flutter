import 'package:flutter/material.dart';
import 'package:scripsf/pages/home/home_account_tab.dart';
import 'package:scripsf/widgets/blank_setting.dart';
import 'package:scripsf/pages/home/home_contact_tab.dart';
import 'package:scripsf/pages/home/home_doctor_tab.dart';
import 'package:scripsf/pages/practices/practice_detail_tab.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/pages/home/home_role_tab.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/app_content.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

class PracticeScreen extends StatelessWidget {

  final List<TabItem> practiceDetailTab = <TabItem>[
    TabItem(title: 'Practice Details', view: PracticeDetailTab()),
    TabItem(title: 'Contact Details', view: BlankSetting()),
    TabItem(title: 'Practice Info', view: BlankSetting()),
  ];

  @override
  Widget build(BuildContext context) {
    return AppContent(screen: TabScreen(tabs: practiceDetailTab));
  }
}
