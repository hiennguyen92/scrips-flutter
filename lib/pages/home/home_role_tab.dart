import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/widgets/setting_list_view.dart';
import 'package:scripsf/widgets/divider_line.dart';
import 'package:scripsf/widgets/header.dart';

class RoleItem {
  final String title;
  final String subTitle;

  RoleItem({this.title, this.subTitle});
}

class HomeRoleTab extends StatelessWidget {

  List<RoleItem> data = <RoleItem>[
    RoleItem(
        title: 'In-house Doctor',
        subTitle: 'In-house doctor has access to the entire system.'),
    RoleItem(
        title: 'Visiting Doctor',
        subTitle:
            'Visiting doctor has access only to their clinical records and patients.'),
    RoleItem(
        title: 'Nurse',
        subTitle:
            'Nurse has access to patient details, vitals, examination and clinical information.'),
    RoleItem(
        title: 'Office Manager',
        subTitle:
            'Office manager has access to practice account and scheduling.'),
    RoleItem(
        title: 'Billing Staff',
        subTitle: 'Billing staff has access to billing and reports.'),
    RoleItem(
        title: 'Receptionist',
        subTitle: 'Receptionist has access to scheduling.'),
  ];

  HomeRoleTab({Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance
        .addPostFrameCallback((_) => onAfterBuild(context));

    return SettingListView<RoleItem>(
        data: data, settingName: 'Roles', buildListItem: (
        BuildContext context, RoleItem item, int index, int length) {
      bool fistItem = index == 0;
      bool lastItem = index == length - 1;
      return Container(
        padding: EdgeInsets.fromLTRB(32, fistItem ? 24 : 0, 0, lastItem ? 24 : 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: fistItem ? Radius.circular(10.0) : Radius.circular(0.0),
            topRight: fistItem ? Radius.circular(10.0) : Radius.circular(0.0),
            bottomLeft: lastItem ? Radius.circular(10.0) : Radius.circular(0.0),
            bottomRight: lastItem ? Radius.circular(10.0) : Radius.circular(0.0),
          ),
          color: enabledBtnTextColor,
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.title ?? 'Empty',
                          style: normalLabelTextStyle(17, regularTextColor),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text(
                            item.subTitle ?? 'Empty',
                            style: semiBoldLabelTextStyle(
                                13, labelTextStyleTextColor),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 24),
                  child: Button(
                    width: 72,
                    height: 32,
                    text: 'Edit',
                    buttonBackgroundColor: textFieldBGcolor,
                    style: semiBoldLabelTextStyle(15, enabledBtnBGColor),
                  ),
                )
              ],
            ),
            (!lastItem)
                ? Column(
              children: [
                Space(vertical: 16),
                DividerLine(),
                Space(vertical: 16)
              ],
            )
                : Container()
          ],
        ),
      );
    },);
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context).add(HeaderInfo(title: 'Roles', breadCrumb: ['Roles']));
    BlocProvider.of<HeaderActionBloc>(context).add(HeaderAction(text: 'Save'));
  }
}
