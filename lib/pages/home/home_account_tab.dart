import 'package:dashed_container/dashed_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

class HomeAccountTab extends StatelessWidget {
  const HomeAccountTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
        child: Container(
      padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                Text('ACCOUNT ID: ',
                    style: boldLabelTextStyle(12, labelTextStyleTextColor)),
                Text('N/A', style: normalLabelTextStyle(13, regularTextColor)),
                SizedBox(width: 23),
                Text('LAST SIGN-IN: ',
                    style: boldLabelTextStyle(12, labelTextStyleTextColor)),
                Text('N/A', style: normalLabelTextStyle(13, regularTextColor)),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 36, bottom: 16),
            child: Text(
              'These settings are available only to you as the Super Admin of this account.',
              style: semiBoldLabelTextStyle(13, labelTextStyleTextColor),
            ),
          ),
          OrgDetailWidget(),
          OrgLicenseWidget(),
          AccountDetailWidget(),
          PasswordWidget(),
          Container(
            constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
            child: Button(
              height: 48,
              radius: 13,
              buttonBackgroundColor: enabledBtnTextColor,
              style: semiBoldLabelTextStyle(17, redButtonTextColor),
              text: "Deactivate This Organization’s Account",
            ),
          ),
        ],
      ),
    ));
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context).add(
        HeaderInfo(title: 'Account Details', breadCrumb: ['Account Details']));
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));
  }
}

class OrgDetailWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
      margin: EdgeInsets.only(top: 8, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormView(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            header: "ORGANIZATION DETAILS",
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Container(
                child: Text("ORGANIZATION LOGO",
                    style: boldLabelTextStyle(12.0, labelTextStyleTextColor)),
              ),
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 25.5),
                child: Stack(
                  children: [
                    Container(
                        width: 80.0,
                        height: 80.0,
                        padding: EdgeInsets.all(7.8),
                        decoration: BoxDecoration(
                          color: gradientColor2,
                          shape: BoxShape.circle,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(AppImages.emptyAvatar))),
                        )),
                    Container(
                      width: 24,
                      child: Image.asset(AppImages.icCamera),
                    )
                  ],
                ),
              ),
              FieldAndLabel(
                labelValue: 'ORGANIZATION NAME',
                fieldValue: 'Primary Clinics FZE',
                margin: EdgeInsets.all(0),
                padding: EdgeInsets.only(left: 0),
                fieldBackgroundColor: textFieldBGcolor,
                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
              ),
              Space(vertical: 26),
              FieldAndLabel(
                labelValue: 'TYPE OF PRACTICE',
                fieldValue: 'Private Practice',
                rightIcon: Image.asset(AppImages.icDropdown),
                margin: EdgeInsets.all(0),
                padding: EdgeInsets.only(left: 0),
                fieldBackgroundColor: textFieldBGcolor,
                labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

class OrgLicenseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
      margin: EdgeInsets.only(top: 8, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormView(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            header: "ORGANIZATION LICENSE",
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FieldAndLabel(
                    labelValue: 'LICENSE TYPE',
                    placeholder: 'Select type',
                    fieldValue: null,
                    rightIcon: Image.asset(AppImages.icDropdown),
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                        boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  FieldAndLabel(
                    labelValue: 'LICENSE NUMBER',
                    placeholder: 'Enter license number',
                    fieldValue: "",
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0, top: 26),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                        boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  FieldAndLabel(
                    labelValue: 'LICENSE EXPIRATION DATE',
                    placeholder: 'Select expiration date',
                    fieldValue: null,
                    fieldType: FieldType.DateRangePicker,
                    icon: Image.asset(AppImages.icCalendar),
                    rightIcon: Image.asset(AppImages.icDropdown),
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0, top: 25.5, bottom: 26),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                        boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  Text(
                    'LICENSE PHOTOS',
                    style: boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    child: Row(
                      children: [
                        PhotoBlockWidget(),
                        Space(horizontal: 16),
                        PhotoBlockWidget(text: 'Back Side'),
                      ],
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class PhotoBlockWidget extends StatelessWidget {
  final String text;

  const PhotoBlockWidget({Key key, this.text = "Front Side"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DashedContainer(
      dashColor: separatorColor,
      borderRadius: 7.0,
      strokeWidth: 1.0,
      child: Container(
        height: 68.22,
        width: 110,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7.0)),
            color: gradientColor2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AppImages.icCamera),
            Text(text, style: normalLabelTextStyle(13, defaultFieldHintColor)),
          ],
        ),
      ),
    );
  }
}

class AccountDetailWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
      margin: EdgeInsets.only(top: 8, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormView(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            header: "ACCOUNT DETAILS",
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FieldAndLabel(
                    labelValue: 'FIRST NAME',
                    placeholder: 'Enter first name',
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                    boldLabelTextStyle(12, labelTextStyleTextColor),
                    fieldValue: 'Jack',
                  ),
                  FieldAndLabel(
                    labelValue: 'LAST NAME',
                    placeholder: 'Enter last name',
                    fieldValue: 'Smith',
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0, top: 26),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                    boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 15),
                          child: FieldAndLabel(
                            labelValue: 'EMAIL ADDRESS',
                            placeholder: 'Enter email address',
                            fieldValue: 'jack.smith@primaryclinics.ae',
                            margin: EdgeInsets.all(0),
                            padding: EdgeInsets.only(left: 0, top: 26),
                            fieldBackgroundColor: textFieldBGcolor,
                            labelTextStyle:
                            boldLabelTextStyle(12, labelTextStyleTextColor),
                          ),
                        ),
                      ),
                      Button(
                        height: 36,
                        width: 70,
                        buttonBackgroundColor: disabledBtnBGColor,
                        text: 'Update',
                        style: semiBoldLabelTextStyle(15, enabledBtnTextColor),
                      )
                    ],
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class PasswordWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
      margin: EdgeInsets.only(top: 8, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormView(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            header: "PASSWORD",
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FieldAndLabel(
                    labelValue: 'CURRENT PASSWORD',
                    placeholder: 'Enter current password',
                    fieldValue: "",
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                        boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  FieldAndLabel(
                    labelValue: 'NEW PASSWORD',
                    placeholder: 'Enter new password',
                    fieldValue: "",
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.only(left: 0, top: 26),
                    fieldBackgroundColor: textFieldBGcolor,
                    labelTextStyle:
                        boldLabelTextStyle(12, labelTextStyleTextColor),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 15),
                          child: FieldAndLabel(
                            labelValue: 'CONFIRM NEW PASSWORD',
                            placeholder: 'Confirm new password',
                            fieldValue: "",
                            margin: EdgeInsets.all(0),
                            padding: EdgeInsets.only(left: 0, top: 26),
                            fieldBackgroundColor: textFieldBGcolor,
                            labelTextStyle:
                                boldLabelTextStyle(12, labelTextStyleTextColor),
                          ),
                        ),
                      ),
                      Button(
                        height: 36,
                        width: 70,
                        buttonBackgroundColor: disabledBtnBGColor,
                        text: 'Update',
                        style: semiBoldLabelTextStyle(15, enabledBtnTextColor),
                      )
                    ],
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
