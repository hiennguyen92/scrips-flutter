import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/indicator_tag_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/utils/app_constant.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:scripsf/widgets/setting_list_view.dart';

class PracticeItem {
  final String title;
  final String location;
  final String time;
  final bool isPrimary;
  final String avatar;

  PracticeItem(
      {this.title,
      this.location,
      this.time,
      this.isPrimary = false,
      this.avatar});

  factory PracticeItem.fromJson(Map<String, dynamic> json) => PracticeItem(
      title: json["title"],
      location: json["location"],
      time: json["time"],
      isPrimary: json["isPrimary"],
      avatar: json["avatar"]);

  Map<String, dynamic> toJson() => {
        "title": title,
        "location": location,
        "time": time,
        "isPrimary": isPrimary,
        "avatar": avatar,
      };
}

class HomePracticeTab extends StatelessWidget {
  List<PracticeItem> data = <PracticeItem>[
    PracticeItem(
        title: 'Primary Practice Dubai',
        location: 'Sheikh Zayed Rd 125',
        time: 'Mon-Fri from 8:00 AM to 5:30 PM',
        isPrimary: true,
        avatar: AppImages.dubaiHospital),
    PracticeItem(
        title: 'Primary Practice Abu Dhabi',
        location: 'Sheikh Maktoum Rd 50',
        time: 'Mon-Fri from 8:00 AM to 5:30 PM',
        avatar: AppImages.abuDhabiHospital),
  ];

  HomePracticeTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SettingListView<PracticeItem>(
      data: data,
      settingName: 'Practices',
      buildListItem:
          (BuildContext context, PracticeItem item, int index, int length) {
        return Card(
          elevation: 0,
          margin: EdgeInsets.only(left: 0, bottom: 8),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(13.0),
          ),
          color: enabledBtnTextColor,
          child: Container(
            child: Row(
              children: [
                Container(
                  margin:
                      EdgeInsets.only(left: 16, top: 12, bottom: 13, right: 16),
                  child: Container(
                    width: 72.0,
                    height: 72.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(item.avatar ?? AppImages.icEmpty),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 16, bottom: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              item.title ?? 'Empty',
                              style:
                                  boldLabelTextStyle(17, defaultFieldTextColor),
                            ),
                            Space(horizontal: 4),
                            item.isPrimary
                                ? IndicatorTagWidget(
                                    indicatorText: "Primary",
                                  )
                                : Container()
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 15,
                                height: 15,
                                child: Image.asset(AppImages.icLocation),
                              ),
                              Space(horizontal: 7),
                              Text(
                                item.location ?? 'Empty',
                                style:
                                    normalLabelTextStyle(15, regularTextColor),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 15,
                                height: 15,
                                child: Image.asset(AppImages.icSmallClock),
                              ),
                              Space(horizontal: 7),
                              Text(
                                item.time ?? 'Empty',
                                style:
                                    normalLabelTextStyle(15, regularTextColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 24),
                  child: Button(
                    width: 72,
                    height: 32,
                    text: 'Edit',
                    buttonBackgroundColor: textFieldBGcolor,
                    style: semiBoldLabelTextStyle(15, enabledBtnBGColor),
                    onPressed: () async {
                      // Save header info
                      await GetIt.I<LocalStorage>().saveHeaderInfo(HeaderInfo(
                          title: item.title,
                          imgPath: item.avatar,
                          breadCrumb: ['practices', item.title]));
                      // Save practice info
                      await GetIt.I<LocalStorage>().savePracticeInfo(
                          PracticeItem(
                              title: item.title,
                              location: item.location,
                              time: item.time,
                              isPrimary: item.isPrimary,
                              avatar: item.avatar));

                      Navigator.pushNamed(
                          context, AppConstant.practiceScreenRouteName,
                          arguments: item);
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context)
        .add(HeaderInfo(title: 'Practices', breadCrumb: ['Practices']));
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Add', onPress: () {}));
  }
}
