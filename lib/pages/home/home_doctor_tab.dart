import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/indicator_tag_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/utils/app_constant.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:scripsf/widgets/setting_list_view.dart';

class DoctorItem {
  final String name;
  final String department;
  final String info;
  final bool isPrimary;
  final String avatar;

  DoctorItem(
      {this.name,
      this.department,
      this.info,
      this.isPrimary = false,
      this.avatar});

  factory DoctorItem.fromJson(Map<String, dynamic> json) => DoctorItem(
      name: json["name"],
      department: json["department"],
      info: json["info"],
      isPrimary: json["isPrimary"],
      avatar: json["avatar"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "department": department,
        "info": info,
        "isPrimary": isPrimary,
        "avatar": avatar,
      };
}

class HomeDoctorTab extends StatelessWidget {
  List<DoctorItem> data = <DoctorItem>[
    DoctorItem(
        name: 'Jack Smith',
        department: 'Neurology',
        info:
            'Primary Practice Dubai  •  jack.smith@email.com  •  Last Active Aug 15 2018 ',
        isPrimary: true,
        avatar: AppImages.jackDoctor),
    DoctorItem(
        name: 'Jane Black',
        department: 'Allergist',
        info:
            'Primary Practice Dubai  •  jane.black@dubaiprimary.ae  •  Last Active Aug 15 2018 ',
        avatar: AppImages.janeDoctor),
    DoctorItem(
        name: 'John Smith',
        department: 'Cardiology',
        info:
            'Primary Practice Dubai  •  john.smith@dubaiprimary.ae  •  Last Active Aug 15 2018 ',
        avatar: AppImages.johnDoctor),
    DoctorItem(
        name: 'Amanda Johnson',
        department: 'Obstetrics & Gynecology',
        info:
            'Primary Practice Dubai  •  amanda.johnson@dubaiprimary.ae  •  Last Active Aug 15 2018 ',
        avatar: AppImages.amandaDoctor),
    DoctorItem(
        name: 'John Smith',
        department: 'Cardiology',
        info:
            'Primary Practice Dubai  •  john.smith@dubaiprimary.ae  •  Last Active Aug 15 2018 ',
        avatar: AppImages.johnDoctor),
    DoctorItem(
        name: 'Amanda Johnson',
        department: 'Obstetrics & Gynecology',
        info:
            'Primary Practice Dubai  •  amanda.johnson@dubaiprimary.ae  •  Last Active Aug 15 2018 ',
        avatar: AppImages.amandaDoctor),
  ];

  HomeDoctorTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SettingListView<DoctorItem>(
      data: data,
      settingName: 'Doctors',
      buildListItem:
          (BuildContext context, DoctorItem item, int index, int length) {
        return Column(
          children: [
            Card(
              elevation: 0,
              margin: EdgeInsets.only(left: 0, bottom: 8),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(13.0),
              ),
              color: enabledBtnTextColor,
              child: Container(
                child: Stack(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 40.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage(
                                    item.avatar ?? AppImages.emptyAvatar),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 16, bottom: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      item.name ?? 'Empty',
                                      style: boldLabelTextStyle(
                                          17, defaultFieldTextColor),
                                    ),
                                    Space(horizontal: 4),
                                    item.isPrimary
                                        ? IndicatorTagWidget(
                                            indicatorText: "Primary",
                                          )
                                        : Container()
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        item.department ?? 'Empty',
                                        style: normalLabelTextStyle(
                                            15, regularTextColor),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8, right: 92),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                          child: Text(
                                        item.info ?? 'Empty',
                                        style: normalLabelTextStyle(
                                            15, regularTextColor),
                                      ))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 96,
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.only(right: 24),
                      child: Button(
                        width: 72,
                        height: 32,
                        text: 'Edit',
                        buttonBackgroundColor: textFieldBGcolor,
                        style: semiBoldLabelTextStyle(15, enabledBtnBGColor),
                        onPressed: () async {
                          // Save header info
                          await GetIt.I<LocalStorage>().saveHeaderInfo(
                              HeaderInfo(
                                  title: item.name,
                                  imgPath: item.avatar,
                                  breadCrumb: ['doctors', item.name]));
                          // Save doctor info
                          await GetIt.I<LocalStorage>().saveDoctorInfo(
                              DoctorItem(
                                  name: item.name,
                                  department: item.department,
                                  info: item.info,
                                  isPrimary: item.isPrimary,
                                  avatar: item.avatar));

                          Navigator.pushNamed(
                              context, AppConstant.doctorScreenRouteName,
                              arguments: item);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            (index == length - 1)
                ? Container(
                    constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                    child: Button(
                      height: 48,
                      radius: 13,
                      buttonBackgroundColor: enabledBtnTextColor,
                      style: semiBoldLabelTextStyle(17, enabledBtnBGColor),
                      text: "Add Another Doctor",
                    ),
                  )
                : Container()
          ],
        );
      },
    );
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context)
        .add(HeaderInfo(title: 'Doctors', breadCrumb: ['Doctors']));
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Add', onPress: () {}));
  }
}
