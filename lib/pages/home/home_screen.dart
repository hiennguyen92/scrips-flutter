import 'package:flutter/material.dart';
import 'package:scripsf/pages/home/home_account_tab.dart';
import 'package:scripsf/pages/home/home_contact_tab.dart';
import 'package:scripsf/pages/home/home_doctor_tab.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/pages/home/home_role_tab.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/app_content.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

class HomeScreen extends StatelessWidget {
  final List<TabItem> homeTab = <TabItem>[
    TabItem(title: 'Account Details', view: HomeAccountTab()),
    TabItem(title: 'Contact Details', view: HomeContactTab()),
    TabItem(title: 'Practices', view: HomePracticeTab()),
    TabItem(title: 'Roles', view: HomeRoleTab()),
    TabItem(title: 'Doctors', view: HomeDoctorTab()),
    TabItem(title: 'Staff', view: HomeStaffTab()),
  ];

  @override
  Widget build(BuildContext context) {
    return AppContent(screen: TabScreen(tabs: homeTab));
  }
}
