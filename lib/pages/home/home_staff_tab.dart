import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/utils/app_constant.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:scripsf/widgets/setting_list_view.dart';

class StaffInfo {
  final String name;
  final String title;
  final String contact;
  final String avatar;

  StaffInfo({this.name, this.title, this.contact, this.avatar});

  factory StaffInfo.fromJson(Map<String, dynamic> json) => StaffInfo(
      name: json["name"],
      title: json["title"],
      contact: json["contact"],
      avatar: json["avatar"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "title": title,
        "contact": contact,
        "avatar": avatar,
      };
}

class StaffItem {
  final String role;
  final List<StaffInfo> staffInfoList;

  StaffItem({this.role, this.staffInfoList});
}

class StaffSection {
  final String header;
  final StaffInfo staffInfo;

  StaffSection({this.header, this.staffInfo});
}

class HomeStaffTab extends StatelessWidget {
  List<StaffItem> data = <StaffItem>[
    StaffItem(role: 'Office Managers', staffInfoList: [
      StaffInfo(
          name: 'Olivia Merritt',
          title: 'Office Manager',
          contact:
              'Primary Practice Dubai  •  olivia.merritt@email.com  •  Last Active Aug 15 2018 ',
          avatar: AppImages.oliviaStaff),
    ]),
    StaffItem(role: 'Nurses', staffInfoList: [
      StaffInfo(
          name: 'Nadine Stacey',
          title: 'Nurse',
          contact:
              'Primary Practice Dubai  •  nadine.stacey@email.com  •  Last Active Aug 15 2018 ',
          avatar: AppImages.nadineStaff),
    ]),
    StaffItem(role: 'Receptionists', staffInfoList: [
      StaffInfo(
          name: 'Rebecca Schmitt',
          title: 'Receptionists',
          contact:
              'Primary Practice Dubai  •  rebecca.schmitt@email.com  •  Pending Invite',
          avatar: AppImages.rebeccaStaff),
    ]),
    StaffItem(role: 'Billing Staff', staffInfoList: [
      StaffInfo(
          name: 'Barbara Sharpe',
          title: 'Billing Staff',
          contact:
              'Primary Practice Dubai  •  rebecca.schmitt@email.com  •  Pending Invite',
          avatar: AppImages.barbaraStaff),
    ]),
  ];

  HomeStaffTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    List<StaffSection> sectionData = getSectionData();
    return SettingListView<StaffSection>(
      data: sectionData,
      settingName: 'Staff',
      buildListItem:
          (BuildContext context, StaffSection item, int index, int length) {
        return item.staffInfo == null
            ? SectionHeaderWidget(headerText: item.header)
            : StaffSectionItemWidget(item: item, index: index, length: length);
      },
    );
  }

  // Build section data list
  List<StaffSection> getSectionData() {
    List<StaffSection> result = [];
    data.forEach((section) {
      result.add(StaffSection(header: section.role));
      section.staffInfoList.forEach((staff) {
        result.add(StaffSection(header: section.role, staffInfo: staff));
      });
    });
    return result;
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context)
        .add(HeaderInfo(title: 'Staff', breadCrumb: ['Staff']));
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Add', onPress: () {}));
  }
}

class SectionHeaderWidget extends StatelessWidget {
  final String headerText;

  const SectionHeaderWidget({Key key, this.headerText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 16),
      child: Text(
        headerText.toUpperCase(),
        style: boldLabelTextStyle(12, defaultFieldTextColor),
      ),
    );
  }
}

class StaffSectionItemWidget extends StatelessWidget {
  final StaffSection item;
  final int index;
  final int length;

  const StaffSectionItemWidget({Key key, this.item, this.index, this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    StaffInfo staffInfo = item.staffInfo;

    return Column(
      children: [
        Card(
          elevation: 0,
          margin: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(13.0),
          ),
          color: enabledBtnTextColor,
          child: Container(
              child: Stack(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 40.0,
                      height: 40.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage(
                              staffInfo.avatar ?? AppImages.emptyAvatar),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 16, bottom: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                staffInfo.name ?? 'Empty',
                                style: boldLabelTextStyle(
                                    17, defaultFieldTextColor),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  staffInfo.title ?? 'Empty',
                                  style: normalLabelTextStyle(
                                      15, regularTextColor),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8, right: 92),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: Text(
                                  staffInfo.contact ?? 'Empty',
                                  style: normalLabelTextStyle(
                                      15, regularTextColor),
                                ))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: 96,
                alignment: Alignment.centerRight,
                margin: EdgeInsets.only(right: 24),
                child: Button(
                  width: 72,
                  height: 32,
                  text: 'Edit',
                  buttonBackgroundColor: textFieldBGcolor,
                  style: semiBoldLabelTextStyle(15, enabledBtnBGColor),
                  onPressed: () async {
                    // Save header info
                    await GetIt.I<LocalStorage>().saveHeaderInfo(HeaderInfo(
                        title: staffInfo.name,
                        imgPath: staffInfo.avatar,
                        breadCrumb: ['Staff', item.header, staffInfo.name]));
                    // Save staff info
                    await GetIt.I<LocalStorage>().saveStaffInfo(StaffInfo(
                        name: staffInfo.name,
                        title: staffInfo.title,
                        contact: staffInfo.contact,
                        avatar: staffInfo.avatar));

                    String routeName = AppConstant.staffDefaultScreenRouteName;
                    if (item.header == 'Nurses') {
                      routeName = AppConstant.staffNurseScreenRouteName;
                    }

                    Navigator.pushNamed(context, routeName,
                        arguments: staffInfo);
                  },
                ),
              ),
            ],
          )),
        ),
        (index == length - 1)
            ? Container(
                margin: EdgeInsets.only(top: 8),
                constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                child: Button(
                  height: 48,
                  radius: 13,
                  buttonBackgroundColor: enabledBtnTextColor,
                  style: semiBoldLabelTextStyle(17, enabledBtnBGColor),
                  text: "Add Another Staff Member",
                ),
              )
            : Container(),
      ],
    );
  }
}
