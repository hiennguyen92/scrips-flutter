import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scripsf/widgets/header.dart';

class HomeContactTab extends StatelessWidget {
  const HomeContactTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance
        .addPostFrameCallback((_) => onAfterBuild(context));

    return Container();
  }

  void onAfterBuild(BuildContext context) async {
    BlocProvider.of<HeaderInfoBloc>(context).add(HeaderInfo(title: 'Contact Details', breadCrumb: ['Contact Details']));
    BlocProvider.of<HeaderActionBloc>(context).add(HeaderAction(text: 'Save'));
  }
}
