import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/utils/utils.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

class NursingSchoolItem {
  final String name;
  final String time;
  final String location;

  NursingSchoolItem({this.name, this.time, this.location});
}

class StaffNurseProfessionalTab extends StatelessWidget {
  final List<NursingSchoolItem> data = <NursingSchoolItem>[
    NursingSchoolItem(
        name: 'British University in Egypt',
        time: 'Nurse of Osteopathic Medicine, 2016',
        location: 'Cairo, Egypt'),
  ];

  StaffNurseProfessionalTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<StaffProfessionalDetailBloc, StaffInfo>(
        builder: (context, staffInfo) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(bottom: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ProfessionalDetailWidget(staffInfo: staffInfo),
                      Space(vertical: 24),
                      NursingSchoolWidget(
                        staffInfo: staffInfo,
                        data: data,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<StaffProfessionalDetailBloc>(context).add(true);
  }
}

enum NursingSchoolEvent { edit, view }

class NursingSchoolBloc extends Bloc<NursingSchoolEvent, NursingSchoolEvent> {
  @override
  NursingSchoolEvent get initialState => NursingSchoolEvent.view;

  @override
  Stream<NursingSchoolEvent> mapEventToState(NursingSchoolEvent event) async* {
    yield event;
  }
}

class StaffProfessionalDetailBloc extends Bloc<bool, StaffInfo> {
  final StaffInfo initialItem = StaffInfo(
      name: 'Empty',
      title: 'Empty',
      contact: 'Contact',
      avatar: AppImages.icEmpty);

  @override
  StaffInfo get initialState => initialItem;

  @override
  Stream<StaffInfo> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getStaffInfo();
  }
}

class ProfessionalDetailWidget extends StatelessWidget {
  final StaffInfo staffInfo;

  const ProfessionalDetailWidget({Key key, this.staffInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormView(
        margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
        header: "PROFESSIONAL DETAILS",
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FieldAndLabel(
              labelValue: 'LICENSE NUMBER',
              isMandatory: true,
              fieldValue: 'LN12345678',
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.only(left: 0),
              fieldBackgroundColor: textFieldBGcolor,
              labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
            ),
            Space(vertical: 26),
            FieldAndLabel(
              isMandatory: true,
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.only(left: 0),
              fieldBackgroundColor: textFieldBGcolor,
              fieldValue: 'DHA',
              tagsItems: [
                ValueDisplayPair('DHA', 'DHA'),
                ValueDisplayPair('HAAD', 'HAAD'),
                ValueDisplayPair('MOH', 'MOH'),
                ValueDisplayPair('EHA', 'EHA'),
                ValueDisplayPair('DHCC', 'DHCC')
              ],
              fieldTextColor: textInputColor,
              fieldType: FieldType.SingleTagPicker,
              labelTextStyle: defaultFieldLabelStyle(null, null),
              labelValue: "LICENSE ISSUING AUTHORITY",
              validationMessage: "",
              axis: Axis.vertical,
              enabled: true,
            ),
            Space(vertical: 26),
            FieldAndLabel(
              labelValue: 'LICENSE EXPIRATION DATE',
              isMandatory: true,
              placeholder: 'Jan 23, 2021',
              fieldValue: null,
              fieldType: FieldType.DateRangePicker,
              icon: Image.asset(AppImages.icCalendar),
              rightIcon: Image.asset(AppImages.icDropdown),
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.only(left: 0),
              fieldBackgroundColor: textFieldBGcolor,
              labelTextStyle: boldLabelTextStyle(12, labelTextStyleTextColor),
            )
          ],
        ));
  }
}

class NursingSchoolWidget extends StatelessWidget {
  final StaffInfo staffInfo;
  final List<NursingSchoolItem> data;

  const NursingSchoolWidget({Key key, this.staffInfo, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NursingSchoolBloc(),
      child: BlocBuilder<NursingSchoolBloc, NursingSchoolEvent>(
        builder: (BuildContext context, NursingSchoolEvent state) {
          return state == NursingSchoolEvent.view
              ? Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'NURSING SCHOOL',
                        style: boldLabelTextStyle(12, defaultFieldTextColor),
                      ),
                      Container(
                        constraints:
                            BoxConstraints(minWidth: 100, maxWidth: 674),
                        margin: EdgeInsets.only(top: 16),
                        child: Column(
                          children: <Widget>[
                            ...List.generate(data.length, (index) {
                              NursingSchoolItem item = data[index];
                              return Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(13.0),
                                ),
                                margin: EdgeInsets.only(bottom: 12),
                                color: enabledBtnTextColor,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      top: 12,
                                      left: 21.78,
                                      right: 24,
                                      bottom: 12),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              item.name,
                                              style: boldLabelTextStyle(
                                                  15, textInputColor),
                                            ),
                                            Space(vertical: 4),
                                            Text(
                                              item.time,
                                              style: normalLabelTextStyle(
                                                  13, regularTextColor),
                                            ),
                                            Space(vertical: 4),
                                            Text(
                                              item.location,
                                              style: normalLabelTextStyle(
                                                  13, regularTextColor),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Button(
                                          width: 72,
                                          height: 32,
                                          text: 'Edit',
                                          buttonBackgroundColor: bgColor,
                                          style: semiBoldLabelTextStyle(
                                              15, enabledBtnBGColor),
                                          onPressed: () {
                                            BlocProvider.of<NursingSchoolBloc>(
                                                    context)
                                                .add(NursingSchoolEvent.edit);
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            Button(
                              height: 48,
                              radius: 13,
                              buttonBackgroundColor: enabledBtnTextColor,
                              style:
                                  semiBoldLabelTextStyle(17, enabledBtnBGColor),
                              text: "Add Another Nursing School",
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : FormView(
                  margin: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
                  header: "NURSING SCHOOL",
                  child: IntrinsicHeight(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              FieldAndLabel(
                                labelValue: 'COUNTRY',
                                placeholder: 'Select country',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'UNIVERSITY',
                                placeholder: 'Select university',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'GRADUATION YEAR',
                                placeholder: 'Select graduation year',
                                fieldValue: null,
                                rightIcon: Image.asset(AppImages.icDropdown),
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 24),
                                child: Row(
                                  children: [
                                    Button(
                                        width: 72,
                                        height: 32,
                                        buttonBackgroundColor:
                                            disabledBtnBGColor,
                                        style: semiBoldLabelTextStyle(
                                            15, enabledBtnTextColor),
                                        text: 'Save',
                                        onPressed: () {
                                          BlocProvider.of<NursingSchoolBloc>(
                                                  context)
                                              .add(NursingSchoolEvent.view);
                                        }),
                                    Space(horizontal: 8),
                                    Button(
                                        width: 72,
                                        height: 32,
                                        buttonBackgroundColor: bgColor,
                                        style: semiBoldLabelTextStyle(
                                            15, normalBtnTextColor),
                                        text: 'Cancel',
                                        onPressed: () {
                                          BlocProvider.of<NursingSchoolBloc>(
                                                  context)
                                              .add(NursingSchoolEvent.view);
                                        })
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Space(horizontal: 39),
                        Expanded(
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FieldAndLabel(
                                  labelValue: 'CITY',
                                  placeholder: 'Select city',
                                  fieldValue: null,
                                  rightIcon: Image.asset(AppImages.icDropdown),
                                  margin: EdgeInsets.all(0),
                                  padding: EdgeInsets.only(left: 0),
                                  fieldBackgroundColor: textFieldBGcolor,
                                  labelTextStyle: boldLabelTextStyle(
                                      12, labelTextStyleTextColor),
                                ),
                                Space(vertical: 26),
                                FieldAndLabel(
                                  labelValue: 'DEGREE',
                                  placeholder: 'Select degree',
                                  fieldValue: null,
                                  rightIcon: Image.asset(AppImages.icDropdown),
                                  margin: EdgeInsets.all(0),
                                  padding: EdgeInsets.only(left: 0),
                                  fieldBackgroundColor: textFieldBGcolor,
                                  labelTextStyle: boldLabelTextStyle(
                                      12, labelTextStyleTextColor),
                                ),
                                Spacer(),
                                Container(
                                  alignment: Alignment.bottomRight,
                                  child: Button(
                                      width: 88,
                                      height: 32,
                                      buttonBackgroundColor: bgColor,
                                      style: semiBoldLabelTextStyle(15, redButtonTextColor),
                                      text: 'Remove',
                                      onPressed: () {
                                        BlocProvider.of<NursingSchoolBloc>(
                                                context)
                                            .add(NursingSchoolEvent.view);
                                      }),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
        },
      ),
    );
  }
}
