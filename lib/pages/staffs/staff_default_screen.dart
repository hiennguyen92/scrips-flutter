import 'package:flutter/material.dart';
import 'package:scripsf/pages/staffs/staff_admin_account_tab.dart';
import 'package:scripsf/pages/staffs/staff_default_personal_tab.dart';
import 'package:scripsf/provider/app_content.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

class StaffDefaultScreen extends StatelessWidget {
  final List<TabItem> staffDefaultDetailTab = <TabItem>[
    TabItem(title: 'Personal Details', view: StaffDefaultPersonalTab()),
    TabItem(title: 'Account Details', view: StaffAdminAccountTab()
//       view: StaffDefaultAccountDetailSetting()
        ),
  ];

  @override
  Widget build(BuildContext context) {
    return AppContent(
        screen: TabScreen(
      tabs: staffDefaultDetailTab,
      defaultIndex: 0,
    ));
  }
}
