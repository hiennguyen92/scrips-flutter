import 'package:flutter/material.dart';
import 'package:scripsf/pages/staffs/staff_default_account_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_personal_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_practice_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_professional_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_schedule_tab.dart';
import 'package:scripsf/provider/app_content.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

class StaffNurseScreen extends StatelessWidget {
  final List<TabItem> staffNurseDetailTab = <TabItem>[
    TabItem(title: 'Personal Details', view: StaffNursePersonalTab()),
    TabItem(
        title: 'Account Details',
//      view: StaffAdminAccountDetailSetting()
        view: StaffDefaultAccountTab()),
    TabItem(
        title: 'Professional Details', view: StaffNurseProfessionalTab()),
    TabItem(title: 'Schedule', view: StaffNurseScheduleTab()),
    TabItem(title: 'Practices', view: StaffNursePracticeTab()),
  ];

  @override
  Widget build(BuildContext context) {
    return AppContent(
        screen: TabScreen(
      tabs: staffNurseDetailTab,
      defaultIndex: 0,
    ));
  }
}
