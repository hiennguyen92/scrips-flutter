import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/blank_setting.dart';
import 'package:scripsf/widgets/header.dart';

class ScheduleItem {
  final String name;
  final String time;
  final String timeIcon;
  final String location;
  final String locationIcon;

  ScheduleItem(
      {this.name, this.time, this.timeIcon, this.location, this.locationIcon});
}

class StaffNurseScheduleTab extends StatelessWidget {
  List<ScheduleItem> data = <ScheduleItem>[
    ScheduleItem(
        name: 'General',
        time: 'Sep 12, 2018 - Sep 12, 2019',
        timeIcon: AppImages.icWorkTime,
        location: 'Primary Clinic Dubai',
        locationIcon: AppImages.dubaiHospital),
    ScheduleItem(
        name: 'Winter Vacation',
        time: 'Dec 20, 2018 - Jan 10, 2019',
        timeIcon: AppImages.icHolidayTime,
        location: 'Primary Clinic Dubai',
        locationIcon: AppImages.dubaiHospital),
  ];

  StaffNurseScheduleTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return BlocBuilder<StaffScheduleDetailBloc, StaffInfo>(
      builder: (context, staffInfo) {
        return (data == null || data.isEmpty)
            ? BlankSetting(
            title: 'Add A Schedule',
            description:
            'Add at least one schedule. Once you\'re done we\'ll take it from there.\nFields marked with a red dot are mandatory.',
            buttonText: 'Add schedule')
            : Container(
          padding: EdgeInsets.fromLTRB(33, 35, 33, 33),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                margin: EdgeInsets.only(bottom: 32),
                child: RichText(
                  text: TextSpan(
                    text:
                    'This nurse’s schedule is linked to their primary provider ',
                    style:
                    semiBoldLabelTextStyle(13, labelTextStyleTextColor),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Jack Smith',
                          style:
                          semiBoldLabelTextStyle(13, normalBtnTextColor)),
                      TextSpan(text: '.'),
                    ],
                  ),
                ),
              ),
              Container(
                constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                child: Text(
                  'WORKING HOURS',
                  style: boldLabelTextStyle(12, defaultFieldTextColor),
                ),
              ),
              Expanded(
                child: Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      final item = data[index];
                      return StaffNurseScheduleItemWidget(
                          item: item, index: index, length: data.length);
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Add', onPress: () {}));

    // Update Body data
    BlocProvider.of<StaffScheduleDetailBloc>(context).add(true);
  }
}

class StaffScheduleDetailBloc extends Bloc<bool, StaffInfo> {
  final StaffInfo initialItem = StaffInfo(
      name: 'Empty',
      title: 'Empty',
      contact: 'Contact',
      avatar: AppImages.icEmpty);

  @override
  StaffInfo get initialState => initialItem;

  @override
  Stream<StaffInfo> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getStaffInfo();
  }
}

class StaffNurseScheduleItemWidget extends StatelessWidget {
  final ScheduleItem item;
  final int index;
  final int length;

  const StaffNurseScheduleItemWidget(
      {Key key, this.item, this.index, this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(13.0),
      ),
      margin: EdgeInsets.only(top: 8),
      color: enabledBtnTextColor,
      child: Container(
        child: Row(
          children: [
            Expanded(
              child: Container(
                margin:
                    EdgeInsets.only(top: 12, bottom: 11, left: 29, right: 23.5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.name ?? 'Empty',
                      style: boldLabelTextStyle(15, defaultFieldTextColor),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 16,
                            height: 16,
                            child: Image.asset(
                                item.timeIcon ?? AppImages.icSmallClock),
                          ),
                          Space(horizontal: 8),
                          Text(
                            item.time ?? 'Empty',
                            style: normalLabelTextStyle(13, regularTextColor),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 16,
                            height: 16,
                            child: CircleAvatar(
                              backgroundImage: AssetImage(
                                  item.locationIcon ?? AppImages.icLocation),
                            ),
                          ),
                          Space(horizontal: 7),
                          Text(
                            item.location ?? 'Empty',
                            style: normalLabelTextStyle(13, regularTextColor),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
