import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/indicator_tag_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/widgets/blank_setting.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

class StaffNursePracticeTab extends StatelessWidget {

  List<PracticeItem> data = <PracticeItem>[
    PracticeItem(
        title: 'Primary Office Dubai',
        location: 'Bonnington Jumeirah Lakes Towers, Dubai, UAE',
        time: '8:00 AM-5:30 PM',
        avatar: AppImages.dubaiHospital),
    PracticeItem(
        title: 'Secondary Office Dubai',
        location: 'Bonnington Jumeirah Lakes Towers, Dubai, UAE',
        time: '8:00 AM-5:30 PM',
        avatar: AppImages.abuDhabiHospital),
  ];

  StaffNursePracticeTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return BlocBuilder<StaffPracticeDetailBloc, StaffInfo>(
      builder: (context, staffInfo) {
        return (data == null || data.isEmpty)
            ? BlankSetting(
            title: 'Add A Practice',
            description:
            'Add at least one practice. Once you\'re done we\'ll take it from there.\nFields marked with a red dot are mandatory.',
            buttonText: 'Add practice')
            : Container(
          padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'NADINE’S PRACTICES',
                      style: boldLabelTextStyle(12, defaultFieldTextColor),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 12),
                  child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      final item = data[index];
                      return StaffNursePracticeItemWidget(item: item, index: index, length: data.length);
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Add', onPress: () {}));

    // Update Body data
    BlocProvider.of<StaffPracticeDetailBloc>(context).add(true);
  }
}

class StaffPracticeDetailBloc extends Bloc<bool, StaffInfo> {
  final StaffInfo initialItem = StaffInfo(
      name: 'Empty',
      title: 'Empty',
      contact: 'Contact',
      avatar: AppImages.icEmpty);

  @override
  StaffInfo get initialState => initialItem;

  @override
  Stream<StaffInfo> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getStaffInfo();
  }
}

class StaffNursePracticeItemWidget extends StatelessWidget {
  final PracticeItem item;
  final int index;
  final int length;

  const StaffNursePracticeItemWidget({Key key, this.item, this.index, this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.only(left: 0, bottom: 8),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(13.0),
      ),
      color: enabledBtnTextColor,
      child: Container(
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(left: 16, top: 12, bottom: 13, right: 16),
              child: Container(
                width: 72.0,
                height: 72.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(item.avatar ?? AppImages.icEmpty),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 16, bottom: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          item.title ?? 'Empty',
                          style: boldLabelTextStyle(17, defaultFieldTextColor),
                        ),
                        Space(horizontal: 4),
                        item.isPrimary
                            ? IndicatorTagWidget(
                          indicatorText: "Primary",
                        )
                            : Container()
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 15,
                            height: 15,
                            child: Image.asset(AppImages.icLocation),
                          ),
                          Space(horizontal: 7),
                          Text(
                            item.location ?? 'Empty',
                            style: normalLabelTextStyle(15, regularTextColor),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 15,
                            height: 15,
                            child: Image.asset(AppImages.icSmallClock),
                          ),
                          Space(horizontal: 7),
                          Text(
                            item.time ?? 'Empty',
                            style: normalLabelTextStyle(15, regularTextColor),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
