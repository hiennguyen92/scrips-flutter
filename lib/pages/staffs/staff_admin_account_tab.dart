import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/resizable_cupertino_switch_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

import '../home/home_staff_tab.dart';

class StaffAdminAccountTab extends StatelessWidget {
  StaffAdminAccountTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<StaffAdminAccountDetailBloc, StaffInfo>(
        builder: (context, staffInfo) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Row(
                    children: [
                      Text('ACCOUNT ID: ',
                          style:
                              boldLabelTextStyle(12, labelTextStyleTextColor)),
                      Text('N/A',
                          style: normalLabelTextStyle(13, regularTextColor)),
                      SizedBox(width: 23),
                      Text('LAST SIGN-IN: ',
                          style:
                              boldLabelTextStyle(12, labelTextStyleTextColor)),
                      Text('N/A / ',
                          style: normalLabelTextStyle(13, regularTextColor)),
                      Text('Pending Invite',
                          style: normalLabelTextStyle(
                              13, labelTextStyleTextColor)),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'Resend Invite',
                            style:
                                semiBoldLabelTextStyle(13, enabledBtnBGColor),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 36, bottom: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormView(
                        margin: EdgeInsets.only(
                            top: 8.5, bottom: 8.5, right: 26, left: 16),
                        header: "ACCOUNT STATUS",
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Set account active',
                                      style: normalLabelTextStyle(
                                          15, regularTextColor)),
                                  SizedCupertinoSwitch(
                                      height: 31,
                                      width: 51,
                                      scale: 1,
                                      value: true)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Space(vertical: 8),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                            'Enabling ‘Set account active’ will let the user log in into the system. We recommend to keep the account active at all times as long as the user is still a member of your organization.',
                            style: semiBoldLabelTextStyle(
                                13, labelTextStyleTextColor)),
                      ),
                      Space(vertical: 28),
                      FormView(
                        margin:
                            EdgeInsets.symmetric(horizontal: 32.5, vertical: 34),
                        header: "ACCOUNT DETAILS",
                        child: Column(
                          children: [
                            IntrinsicHeight(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        FieldAndLabel(
                                          labelValue: 'ROLE',
                                          isMandatory: false,
                                          fieldValue: 'Nurse',
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.only(left: 0),
                                          fieldBackgroundColor:
                                              textFieldBGcolor,
                                          rightIcon:
                                              Image.asset(AppImages.icDropdown),
                                          labelTextStyle: boldLabelTextStyle(
                                              12, labelTextStyleTextColor),
                                        ),
                                        Space(vertical: 26),
                                        FieldAndLabel(
                                          labelValue: 'EMAIL ADDRESS',
                                          placeholder: 'Enter email address',
                                          isMandatory: false,
                                          fieldValue:
                                              'n.stacey@dubaiprimary.ae',
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.only(left: 0),
                                          fieldBackgroundColor:
                                              textFieldBGcolor,
                                          labelTextStyle: boldLabelTextStyle(
                                              12, labelTextStyleTextColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Space(horizontal: 39),
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          FieldAndLabel(
                                            labelValue:
                                                'ASSIGN TO PRIMARY DOCTOR',
                                            placeholder: 'Enter primary doctor',
                                            fieldValue: 'John Doe',
                                            rightIcon: Image.asset(
                                                AppImages.icDropdown),
                                            margin: EdgeInsets.all(0),
                                            padding: EdgeInsets.only(left: 0),
                                            fieldBackgroundColor:
                                                textFieldBGcolor,
                                            labelTextStyle: boldLabelTextStyle(
                                                12, labelTextStyleTextColor),
                                          ),
                                          Space(vertical: 26),
                                          FieldAndLabel(
                                            labelValue: 'CONTACT NUMBER',
                                            placeholder:
                                                'e.g. +971 23 345 6789',
                                            fieldValue: "",
                                            margin: EdgeInsets.all(0),
                                            padding: EdgeInsets.only(left: 0),
                                            fieldBackgroundColor:
                                                textFieldBGcolor,
                                            labelTextStyle: boldLabelTextStyle(
                                                12, labelTextStyleTextColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Space(vertical: 24.5),
                            Container(
                              height: 48,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        'Assign this staff member to Dubai Primary Practice',
                                        style: normalLabelTextStyle(
                                            15, regularTextColor)),
                                    SizedCupertinoSwitch(
                                        height: 31,
                                        width: 51,
                                        scale: 1,
                                        value: true)
                                  ],
                                ),
                            ),
                            Container(
                              height: 48,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      'Assign this staff member to Abu Dhabi Primary Practice',
                                      style: normalLabelTextStyle(
                                          15, regularTextColor)),
                                  SizedCupertinoSwitch(
                                      height: 31,
                                      width: 51,
                                      scale: 1,
                                      value: false)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Button(
                    height: 48,
                    radius: 13,
                    buttonBackgroundColor: enabledBtnTextColor,
                    style: semiBoldLabelTextStyle(17, redButtonTextColor),
                    text: "Archive This Staff",
                  ),
                ),
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  margin: EdgeInsets.only(top: 16),
                  child: RichText(
                    text: TextSpan(
                      text:
                          'Archiving will remove this staff from active view but will not delete the staff. You can find archived staffs  in the ',
                      style:
                          semiBoldLabelTextStyle(13, labelTextStyleTextColor),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Archive',
                            style:
                                semiBoldLabelTextStyle(13, normalBtnTextColor)),
                        TextSpan(text: ' and unarchive, if necessary.'),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<StaffAdminAccountDetailBloc>(context).add(true);
  }
}

class StaffAdminAccountDetailBloc extends Bloc<bool, StaffInfo> {
  final StaffInfo initialItem = StaffInfo(
      name: 'Empty',
      title: 'Empty',
      contact: 'Contact',
      avatar: AppImages.icEmpty);

  @override
  StaffInfo get initialState => initialItem;

  @override
  Stream<StaffInfo> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getStaffInfo();
  }
}
