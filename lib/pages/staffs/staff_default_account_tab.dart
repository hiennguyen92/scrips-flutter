import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scrips_core/widgets/general/field_and_label.dart';
import 'package:scrips_core/widgets/general/form_view_widget.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/provider/local_storage.dart';
import 'package:scripsf/utils/app_asset.dart';
import 'package:scripsf/widgets/header.dart';

class StaffDefaultAccountTab extends StatelessWidget {
  StaffDefaultAccountTab({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => onAfterBuild(context));

    return SingleChildScrollView(
      child: BlocBuilder<StaffDefaultAccountDetailBloc, StaffInfo>(
        builder: (context, staffInfo) {
          return Container(
            padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormView(
                          margin: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 24),
                          header: "ACCOUNT DETAILS",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FieldAndLabel(
                                labelValue: 'CONTACT NUMBER',
                                placeholder: 'e.g. +971 23 345 6789',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: FieldAndLabel(
                                        labelValue: 'EMAIL ADDRESS',
                                        placeholder: 'Enter email address',
                                        fieldValue: 'n.stacey@dubaiprimary.ae',
                                        margin: EdgeInsets.all(0),
                                        padding: EdgeInsets.only(left: 0),
                                        fieldBackgroundColor: textFieldBGcolor,
                                        labelTextStyle: boldLabelTextStyle(
                                            12, labelTextStyleTextColor),
                                      ),
                                    ),
                                  ),
                                  Button(
                                    height: 36,
                                    width: 70,
                                    buttonBackgroundColor: disabledBtnBGColor,
                                    text: 'Update',
                                    style: semiBoldLabelTextStyle(
                                        15, enabledBtnTextColor),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      Space(vertical: 24),
                      FormView(
                          margin: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 26),
                          header: "PASSWORD",
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FieldAndLabel(
                                labelValue: 'CURRENT PASSWORD',
                                placeholder: 'Enter current password',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              FieldAndLabel(
                                labelValue: 'NEW PASSWORD',
                                placeholder: 'Enter new password',
                                fieldValue: "",
                                margin: EdgeInsets.all(0),
                                padding: EdgeInsets.only(left: 0),
                                fieldBackgroundColor: textFieldBGcolor,
                                labelTextStyle: boldLabelTextStyle(
                                    12, labelTextStyleTextColor),
                              ),
                              Space(vertical: 26),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: FieldAndLabel(
                                        labelValue: 'CONFIRM NEW PASSWORD',
                                        placeholder: 'Confirm new password',
                                        fieldValue: "",
                                        margin: EdgeInsets.all(0),
                                        padding: EdgeInsets.only(left: 0),
                                        fieldBackgroundColor: textFieldBGcolor,
                                        labelTextStyle: boldLabelTextStyle(
                                            12, labelTextStyleTextColor),
                                      ),
                                    ),
                                  ),
                                  Button(
                                    height: 36,
                                    width: 70,
                                    buttonBackgroundColor: disabledBtnBGColor,
                                    text: 'Update',
                                    style: semiBoldLabelTextStyle(
                                        15, enabledBtnTextColor),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void onAfterBuild(BuildContext context) async {
    // Get Header Info from local
    HeaderInfo headerInfo = await GetIt.I<LocalStorage>().getHeaderInfo();
    BlocProvider.of<HeaderInfoBloc>(context).add(headerInfo);
    BlocProvider.of<HeaderActionBloc>(context)
        .add(HeaderAction(text: 'Save', backgroundColor: disabledBtnBGColor));

    // Update Body data
    BlocProvider.of<StaffDefaultAccountDetailBloc>(context).add(true);
  }
}

class StaffDefaultAccountDetailBloc extends Bloc<bool, StaffInfo> {
  final StaffInfo initialItem = StaffInfo(
      name: 'Empty',
      title: 'Empty',
      contact: 'Contact',
      avatar: AppImages.icEmpty);

  @override
  StaffInfo get initialState => initialItem;

  @override
  Stream<StaffInfo> mapEventToState(bool event) async* {
    yield await GetIt.I<LocalStorage>().getStaffInfo();
  }
}
