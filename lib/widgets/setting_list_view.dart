import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';

import 'blank_setting.dart';

class SettingListView<T> extends StatelessWidget {
  final String settingName; //Capitalize
  final List<T> data;
  final Widget Function(BuildContext context, T item, int index, int length)
      buildListItem;
  final Widget bottomView;

  const SettingListView(
      {Key key,
      this.data,
      this.settingName,
      this.buildListItem,
      this.bottomView})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SettingListViewBloc(),
      child: BlocBuilder<SettingListViewBloc, bool>(
        builder: (BuildContext context, isActive) {
          return isActive
              ? SettingListWidget(
                  panelTitle: 'ACTIVE ${settingName.toUpperCase()}',
                  actionText: 'View Archived $settingName',
                  actionPress: () {
                    context.bloc<SettingListViewBloc>().add(false);
                  },
                  data: data,
                  buildListItem: buildListItem,
                  bottomView: bottomView,
                  settingName: settingName,
                )
              : SettingListWidget(
                  panelTitle: 'ARCHIVED ${settingName.toUpperCase()}',
                  actionText: 'View Active $settingName',
                  actionPress: () {
                    context.bloc<SettingListViewBloc>().add(true);
                  },
                  data: data.sublist(0, data.length ~/ 2),
                  buildListItem: buildListItem,
                  bottomView: bottomView,
                  settingName: settingName,
                );
        },
      ),
    );
  }
}

class SettingListViewBloc extends Bloc<bool, bool> {
  @override
  bool get initialState => true;

  @override
  Stream<bool> mapEventToState(bool event) async* {
    yield event;
  }
}

class SettingListWidget<T> extends StatelessWidget {
  final String panelTitle;
  final String actionText;
  final Function() actionPress;
  final List<T> data;
  final String settingName;
  final Widget Function(BuildContext context, T item, int index, int length)
      buildListItem;
  final Widget bottomView;

  const SettingListWidget(
      {Key key,
      @required this.panelTitle,
      @required this.actionText,
      @required this.actionPress,
      @required this.data,
      @required this.settingName,
      @required this.buildListItem,
      @required this.bottomView})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingNameWithouts =
        settingName.substring(0, settingName.length - 1);
    return (data == null || data.isEmpty)
        ? BlankSetting(
            title: 'Add A $settingNameWithouts',
            description:
                'Add at least one ${settingNameWithouts.toLowerCase()}. Once you\'re done we\'ll take it from there.\nFields marked with a red dot are mandatory.',
            buttonText: 'Add $settingNameWithouts')
        : SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          panelTitle,
                          style: boldLabelTextStyle(12, defaultFieldTextColor),
                        ),
                        InkWell(
                          child: Text(
                            actionText,
                            style:
                                semiBoldLabelTextStyle(15, normalBtnTextColor),
                          ),
                          onTap: actionPress ?? () {},
                        )
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    constraints: BoxConstraints(minWidth: 100, maxWidth: 674),
                    margin: EdgeInsets.only(top: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(data.length, (index) {
                        final item = data[index];
                        return buildListItem(context, item, index, data.length);
                      }),
                    ),
                  ),
                  bottomView ?? Container()
                ],
              ),
            ),
          );
  }
}
