import 'package:flutter/material.dart';
import 'package:scripsf/utils/app_asset.dart';

class CircleImage extends StatelessWidget {
  final image;
  final size;

  const CircleImage({Key key, this.image, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size ?? 40,
        height: size ?? 40,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(image ?? AppImages.emptyAvatar))));
  }
}
