
import 'package:flutter/material.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';

class AppBarPadding extends StatelessWidget {
  final Widget background;
  final Widget child;

  const AppBarPadding({Key key, this.child, this.background}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Stack(
      children: [
        background ?? Container(color: enabledBtnTextColor),
        Container(
          padding: EdgeInsets.only(top: statusBarHeight),
          child: child ?? Container(),
        )
      ],
    );
  }
}
