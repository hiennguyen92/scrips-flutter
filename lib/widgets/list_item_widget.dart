import 'package:flutter/material.dart';

class ListItemWidget<T> extends StatelessWidget {
  final T item;
  final int index;
  final int length;
  final Widget child;

  const ListItemWidget(
      {Key key, this.item, this.index, this.length, this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
