import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';

class BlankSetting extends StatelessWidget {
  final String title;
  final String description;
  final String buttonText;

  const BlankSetting({Key key, this.title, this.description, this.buttonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(33, 36, 33, 33),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              title ?? '',
              style: boldLabelTextStyle(17, defaultFieldTextColor),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 24),
            child: Text(
              description ?? '',
              style: normalLabelTextStyle(15, regularTextColor),
            ),
          ),
          Container(
            child: buttonText != null
                ? Button(
                    height: 32,
                    width: 101,
                    text: buttonText,
                    style: semiBoldLabelTextStyle(15, enabledBtnTextColor),
                    buttonBackgroundColor: enabledBtnBGColor,
                  )
                : null,
          )
        ],
      ),
    );
  }
}
