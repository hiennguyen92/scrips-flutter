import 'package:flutter/material.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/space.dart';
import 'package:scripsf/utils/app_asset.dart';

class Breadcrumb extends StatefulWidget {
  List<String> data;

  Breadcrumb({Key key, this.data}) : super(key: key) {
    if (data == null) {
      data = <String>[];
    }
  }

  @override
  _BreadcrumbState createState() => _BreadcrumbState();
}

class _BreadcrumbState extends State<Breadcrumb> {
  @override
  Widget build(BuildContext context) {
    final data = ['settings', ...widget.data];
    return Container(
      margin: EdgeInsets.only(left: 24),
      child: Row(
        children: List.generate(data.length, (index) {
          final String item = data[index];
          final text = Text('${item?.toUpperCase()}',
              style: boldLabelTextStyle(12, labelTextStyleTextColor));
          if (index < data.length - 1) {
            return Row(
              children: [
                text,
                Space(horizontal: 5),
                Container(
                    child: Image.asset(
                      AppImages.icBreadCrumbNext,
                      height: 16,
                      width: 16,
                    )),
                Space(horizontal: 5),
              ],
            );
          } else {
            return text;
          }
        }),
      ),
    );
  }
}
