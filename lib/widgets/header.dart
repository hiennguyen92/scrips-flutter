import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scrips_core/ui_helpers/text_styles.dart';
import 'package:scrips_core/widgets/general/button.dart';
import 'package:scripsf/utils/app_asset.dart';

import 'breadcrumb.dart';
import 'circle_image.dart';

/**
 * How to update info to Header
    BlocProvider.of<HeaderInfoBloc>(context).add(HeaderInfo('Empty Header', null, []));
    BlocProvider.of<HeaderActionBloc>(context).add(HeaderAction('Save', null));
 */

class Header extends StatelessWidget {
  const Header({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 24, bottom: 24),
      color: enabledBtnTextColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: BlocBuilder<HeaderInfoBloc, HeaderInfo>(
              builder: (context, headerInfo) {
                return Container(
                  padding: EdgeInsets.only(top: 29),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Breadcrumb(data: headerInfo.breadCrumb),
                      Container(
                          height: 40,
                          margin: EdgeInsets.only(top: 11),
                          child: Row(
                            children: [
                              Navigator.canPop(context)
                                  ? InkWell(
                                      child: Container(
                                        height: 24,
                                        width: 24,
                                        margin: EdgeInsets.only(
                                            left: 17, right: 10),
                                        alignment: Alignment.centerLeft,
                                        child: Image.asset(AppImages.icBack),
                                      ),
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  : Container(
                                      padding: EdgeInsets.only(left: 24),
                                    ),
                              headerInfo.imgPath != null
                                  ? Container(
                                      width: 40,
                                      height: 40,
                                      margin: EdgeInsets.only(right: 15.5),
                                      child: CircleImage(
                                          image: headerInfo.imgPath, size: 40,),
                                    )
                                  : Container(),
                              Text(
                                headerInfo.title ?? '',
                                style: boldLabelTextStyle(
                                    24, defaultFieldTextColor),
                              ),
                            ],
                          )),
                    ],
                  ),
                );
              },
            ),
          ),
          BlocBuilder<HeaderActionBloc, HeaderAction>(
            builder: (context, headerAction) {
              return headerAction.text != null
                  ? Container(
                      margin: EdgeInsets.only(top: 28),
                      child: Button(
                        height: 32,
                        width: 72,
                        text: headerAction.text,
                        buttonBackgroundColor: headerAction.backgroundColor ??
                            enabledBtnBGColor.withAlpha(
                                headerAction.onPress != null ? 255 : 10),
                        style: headerAction.style ??
                            semiBoldLabelTextStyle(15, enabledBtnTextColor),
                        onPressed: headerAction.onPress ?? () {},
                      ),
                    )
                  : Container();
            },
          ),
        ],
      ),
    );
  }
}

class HeaderInfo {
  final String title;
  final String imgPath;
  final List<String> breadCrumb;

  HeaderInfo({this.title, this.imgPath, this.breadCrumb});

  factory HeaderInfo.fromJson(Map<String, dynamic> json) => HeaderInfo(
      title: json["title"],
      imgPath: json["imgPath"],
      breadCrumb: List<String>.from(json["breadCrumb"].map((x) => x)));

  Map<String, dynamic> toJson() => {
        "title": title,
        "imgPath": imgPath,
        "breadCrumb": List<dynamic>.from(breadCrumb.map((x) => x)),
      };
}

class HeaderInfoBloc extends Bloc<HeaderInfo, HeaderInfo> {
  @override
  HeaderInfo get initialState => HeaderInfo(title: '');

  @override
  Stream<HeaderInfo> mapEventToState(HeaderInfo event) async* {
    yield event;
  }
}

class HeaderAction {
  final String text;
  final Function onPress;
  final TextStyle style;
  final Color backgroundColor;

  HeaderAction({this.text, this.onPress, this.style, this.backgroundColor});
}

class HeaderActionBloc extends Bloc<HeaderAction, HeaderAction> {
  @override
  HeaderAction get initialState => HeaderAction();

  @override
  Stream<HeaderAction> mapEventToState(HeaderAction event) async* {
    yield event;
  }
}
