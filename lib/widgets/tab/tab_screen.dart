import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrips_core/ui_helpers/app_colors.dart';
import 'package:scripsf/widgets/divider_line.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:scripsf/widgets/screen_widget.dart';
import 'package:scripsf/widgets/tab/tab_item.dart';
import 'package:scripsf/widgets/tab/tab_widget.dart';

class CustomTabController extends TabController {

  @override
  void animateTo(int value, {Duration duration = kTabScrollDuration, Curve curve = Curves.ease}) {
    super.animateTo(value, duration: null, curve: null);
  }
}

class TabScreen extends StatefulWidget {
  final List<TabItem> tabs;
  final int defaultIndex;

  TabScreen({Key key, this.tabs, this.defaultIndex = 0}) : super(key: key);

  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: widget.tabs.length);

    // Default tab selected
    final defaultIndex = widget.defaultIndex;
    _tabController.index =
        defaultIndex >= _tabController.length ? 0 : defaultIndex;

    _tabController.addListener(() {
      print(_tabController.index);
      BlocProvider.of<TabScreenBloc>(context).add(_tabController.index);
    });

  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final tabs = widget.tabs;
    return Scaffold(
      body: ScreenWidget(
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) =>
              Container(
            color: bgColor,
            child: Column(children: <Widget>[
              Header(),
              DividerLine(),
              TabWidget(
                width: constraints.maxWidth,
                tabController: _tabController,
                tabs: tabs,
              ),
              DividerLine(),
              Expanded(
                child: _body(),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  Widget _body() {
    return BlocBuilder<TabScreenBloc, int>(
      builder: (BuildContext context, int tabIndex) {
        return Container(
          alignment: Alignment.topLeft,
          child: widget.tabs[tabIndex].view,
        );
      },
    );
  }
}

class TabScreenBloc extends Bloc<int, int> {
  @override
  int get initialState => 0;

  @override
  Stream<int> mapEventToState(int event) async* {
    yield event;
  }
}