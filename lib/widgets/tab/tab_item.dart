import 'package:flutter/material.dart';

class TabItem {
  final String title;
  final Widget view;

  TabItem({this.title, this.view});
}
