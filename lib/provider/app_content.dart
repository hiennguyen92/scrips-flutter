import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scripsf/pages/doctors/doctor_admin_account_detail_setting.dart';
import 'package:scripsf/pages/doctors/doctor_default_account_detail_setting.dart';
import 'package:scripsf/pages/doctors/doctor_personal_detail_setting.dart';
import 'package:scripsf/pages/practices/practice_detail_tab.dart';
import 'package:scripsf/pages/staffs/staff_admin_account_tab.dart';
import 'package:scripsf/pages/staffs/staff_default_account_tab.dart';
import 'package:scripsf/pages/staffs/staff_default_personal_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_personal_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_practice_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_professional_tab.dart';
import 'package:scripsf/pages/staffs/staff_nurse_schedule_tab.dart';
import 'package:scripsf/widgets/appbar_padding.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:scripsf/widgets/tab/tab_screen.dart';

class AppContent extends StatelessWidget {
  final Widget screen;

  const AppContent({Key key, this.screen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: AnnotatedRegion(
        value: SystemUiOverlayStyle.dark,
        child: AppBarPadding(
          child: MultiBlocProvider(providers: [
            BlocProvider<TabScreenBloc>(
              create: (BuildContext context) => TabScreenBloc(),
            ),
            BlocProvider<HeaderInfoBloc>(
              create: (BuildContext context) => HeaderInfoBloc(),
            ),
            BlocProvider<HeaderActionBloc>(
              create: (BuildContext context) => HeaderActionBloc(),
            ),
            BlocProvider<PracticeDetailBloc>(
              create: (BuildContext context) => PracticeDetailBloc(),
            ),
            BlocProvider<DoctorAdminAccountDetailBloc>(
              create: (BuildContext context) => DoctorAdminAccountDetailBloc(),
            ),
            BlocProvider<DoctorDefaultAccountDetailBloc>(
              create: (BuildContext context) =>
                  DoctorDefaultAccountDetailBloc(),
            ),
            BlocProvider<DoctorPersonalDetailBloc>(
              create: (BuildContext context) => DoctorPersonalDetailBloc(),
            ),
            BlocProvider<StaffAdminAccountDetailBloc>(
              create: (BuildContext context) => StaffAdminAccountDetailBloc(),
            ),
            BlocProvider<StaffDefaultAccountDetailBloc>(
              create: (BuildContext context) => StaffDefaultAccountDetailBloc(),
            ),
            BlocProvider<StaffDefaultPersonalDetailBloc>(
              create: (BuildContext context) =>
                  StaffDefaultPersonalDetailBloc(),
            ),
            BlocProvider<StaffNursePersonalDetailBloc>(
              create: (BuildContext context) => StaffNursePersonalDetailBloc(),
            ),
            BlocProvider<StaffPracticeDetailBloc>(
              create: (BuildContext context) => StaffPracticeDetailBloc(),
            ),
            BlocProvider<StaffProfessionalDetailBloc>(
              create: (BuildContext context) => StaffProfessionalDetailBloc(),
            ),
            BlocProvider<StaffScheduleDetailBloc>(
              create: (BuildContext context) => StaffScheduleDetailBloc(),
            ),
          ], child: screen),
        ),
      ),
    );
  }
}
