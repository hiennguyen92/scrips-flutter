import 'dart:convert';

import 'package:scripsf/pages/home/home_doctor_tab.dart';
import 'package:scripsf/pages/home/home_practice_tab.dart';
import 'package:scripsf/pages/home/home_staff_tab.dart';
import 'package:scripsf/widgets/header.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  final String _headerInfoKey = 'header_info';
  final String _doctorInfoKey = 'doctor_info';
  final String _practiceInfoKey = 'practice_info';
  final String _staffInfoKey = 'staff_info';

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // Header
  Future<bool> saveHeaderInfo(HeaderInfo headerInfo) async {
    final SharedPreferences prefs = await _prefs;
    String headerInfoJson = jsonEncode(headerInfo.toJson());
    return await prefs.setString(_headerInfoKey, headerInfoJson);
  }

  Future<HeaderInfo> getHeaderInfo() async {
    final SharedPreferences prefs = await _prefs;
    String headerInfoJson = prefs.getString(_headerInfoKey);
    if (headerInfoJson != null) {
      return HeaderInfo.fromJson(jsonDecode(headerInfoJson));
    }
    return null;
  }

  // Doctor
  Future<bool> saveDoctorInfo(DoctorItem doctorItem) async {
    final SharedPreferences prefs = await _prefs;
    String doctorInfoJson = jsonEncode(doctorItem.toJson());
    return await prefs.setString(_doctorInfoKey, doctorInfoJson);
  }

  Future<DoctorItem> getDoctorInfo() async {
    final SharedPreferences prefs = await _prefs;
    String doctorInfoJson = prefs.getString(_doctorInfoKey);
    if (doctorInfoJson != null) {
      return DoctorItem.fromJson(jsonDecode(doctorInfoJson));
    }
    return null;
  }

  // Practice
  Future<bool> savePracticeInfo(PracticeItem practiceItem) async {
    final SharedPreferences prefs = await _prefs;
    String practiceInfoJson = jsonEncode(practiceItem.toJson());
    return await prefs.setString(_practiceInfoKey, practiceInfoJson);
  }

  Future<PracticeItem> getPracticeInfo() async {
    final SharedPreferences prefs = await _prefs;
    String practiceInfoJson = prefs.getString(_practiceInfoKey);
    if (practiceInfoJson != null) {
      return PracticeItem.fromJson(jsonDecode(practiceInfoJson));
    }
    return null;
  }

  // Staff
  Future<bool> saveStaffInfo(StaffInfo staffItem) async {
    final SharedPreferences prefs = await _prefs;
    String staffInfoJson = jsonEncode(staffItem.toJson());
    return await prefs.setString(_staffInfoKey, staffInfoJson);
  }

  Future<StaffInfo> getStaffInfo() async {
    final SharedPreferences prefs = await _prefs;
    String staffInfoJson = prefs.getString(_staffInfoKey);
    if (staffInfoJson != null) {
      return StaffInfo.fromJson(jsonDecode(staffInfoJson));
    }
    return null;
  }
}
