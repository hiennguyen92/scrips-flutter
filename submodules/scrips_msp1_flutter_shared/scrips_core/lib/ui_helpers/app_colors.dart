import 'package:flutter/material.dart';

const Color lightGrey = Color.fromARGB(255, 61, 63, 69);
final Color red = Colors.red[400];

final Color bgColor = Color(0xFFF4F7F8); //getColorFromHex('#F4F7F8');
final Color separatorColor = Color(0xFFEFEEEE); //getColorFromHex('#EFEEEE');
final Color enabledBtnTextColor =
    Color(0xFFFFFFFF); //getColorFromHex('#FFFFFF');
final Color enabledBtnBGColor = Color(0xFF00B8E3); //getColorFromHex('#00B8E3');

final Color disabledBtnTextColor =
    Color(0xFFFFFFFF); //getColorFromHex('#FFFFFF');
final Color tabBgColor = Color(0xFFEFEFF0);
final Color disabledBtnBGColor =
    Color(0xFFD1D1D1); //getColorFromHex('#D1D1D1');

final Color gradientColor1 = Color(0xFFEEEEEE);
final Color gradientColor2 = Color(0xFFF7F7F7);

final Color blackColor = Color(0xFF000000);

final Color normalBtnTextColor =
    Color(0xFF00B8E3); //getColorFromHex('#00B8E3');
final Color disabledTabTextColor = Color(0xFFB7B7B7);
final Color textInputColor = Color(0xFF233D93); //getColorFromHex('#233D93');
final Color textFieldBGcolor = Color(0xFFF4F7F8); //getColorFromHex('#F4F7F8');

final Color labelTextStyleTextColor =
    Color(0xFFAAAFC0); //getColorFromHex('#AAAFC0');

final Color successTabColor = Color(0xFF65D24A);

final Color regularTextColor = Color(0xFF485685);
final Color searchBGColour = Color(0xFF8E8E93).withOpacity(0.12); //

// mainMenuBackground
const Color selectedMenuBackgroundColor = Color(0xFF2F489A);
const Color defaultMainMenuBackgroundColor = Color(0xFF1F3888);
const Color defaultMainSubMenuBackgroundColor = defaultMainMenuBackgroundColor;
const Color defaultMainAppBarBackgroundColor = defaultMainMenuBackgroundColor;
const Color defaultMainStatusBarBackgroundColor =
    defaultMainMenuBackgroundColor;
const Color mainContainedAreaBackgroundColor = Color(0xFFF4F7F8);
final Color defaultPanelBackgroundColor = Colors.white;
final Color defaultMainBackgroundColor = mainContainedAreaBackgroundColor;

// Content
const Color defaultHeaderBackgroundColor = defaultMainAppBarBackgroundColor;
const Color defaultSubHeaderBackgroundColor = defaultMainAppBarBackgroundColor;
const Color defaultLabelBackgroundColor = Colors.transparent;
const Color defaultValidationBackgroundColor = Colors.transparent;
const Color defaultFieldBackgroundColor = Colors.transparent;
const Color defaultReversedLabelBackgroundColor = Colors.transparent;
const Color defaultReversedValidationBackgroundColor = Colors.transparent;
const Color defaultReversedFieldBackgroundColor = Colors.transparent;
//
const Color defaultHeaderTextColor = Colors.white;
const Color defaultSubHeaderTextColor = Colors.white;
const Color defaultLabelTextColor = Colors.black;
const Color defaultFieldTextColor = Color(0xFF233D93);
const Color defaultReversedLabelTextColor = Colors.white;
const Color defaultReversedFieldTextColor = Colors.white;
const Color defaultFieldHintColor = Color(0xFFB7B7B7);
const Color defaultValidationTextColor = Colors.red;
const Color defaultReversedValidationTextColor = Colors.white;

// red
const Color darkGrey = Color.fromARGB(255, 18, 18, 19);

// pink
const Color primaryColor = Color.fromARGB(255, 9, 202, 172);

const Color backgroundColor = Colors.lightBlue;

// light yellow
const Color commentColor = Color.fromARGB(255, 255, 246, 196);

const Color redButtonTextColor = Color(0xFFEE6464);